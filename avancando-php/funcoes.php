<?php
/*Funcões*/
function escreva($param) {
    echo 'Nome: '. $param . '<br>';
}

$param  = 'Marcelo';

escreva($param);


//Funções - Retorno

function soma_retorno($a, $b) {
    return $a + $b;
}

$res = soma_retorno(2, 3);
$mult = 2;

echo "A soma dos valores é: ". $res . "<br>";

echo "O produto da soma por " . $mult . " é :". $res * $mult;
