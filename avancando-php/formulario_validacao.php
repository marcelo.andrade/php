<html>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
      integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu"
      crossorigin="anonymous">

<head>
    <title>Formulário</title>
</head>






<body>
<div class="container">
<h1>Formulário com PHP</h1>
<hr>
<form action="recebe_fomulario_validacao.php" method="post">
    <div class="form-group">
        <label for="nome" class="col-md-6">Nome:
            <input type="text" name="nome" class="form-control">
        </label>
        <label for="email" class="col-md-6">E-mail:
            <input type="text" name="email" class="form-control">
        </label>
    </div>

    <hr>

    <div class="form-group">
        <fieldset class="col-md-12">
            <legend>Área de interesse</legend>
            <label>
                <input type="checkbox" name="interesse[]" value="informatica">
            </label>Informática

            <br />

            <label>
                <input type="checkbox" name="interesse[]" value="esporte">
            </label>Esporte
            <br />
            <label>
                <input type="checkbox" name="interesse[]" value="compras">
            </label>Compras

            <br />

            <label>
                <input type="checkbox" name="interesse[]" value="moda">
            </label>Moda

            <br />

            <label>
                <input type="checkbox" name="interesse[]" value="ciencia">
            </label>Ciência

            <br />

            <label>
                <input type="checkbox" name="interesse[]" value="religiao">
            </label>Religião
            <br />
        </fieldset>
    </div>

    <br />

    <label for="">Onde conheceu?</label>
    <select name="onde_conheceu" id="">
        <option value="0">Selecione</option>
        <option value="google">Gooole</option>
        <option value="amigos">Amigos</option>
        <option value="revista">Revista</option>
        <option value="tv">TV</option>
    </select>

    <br /><br />

    <label for="">Mensagem</label> <br />
    <textarea name="mensagem" id="" cols="30" rows="10"></textarea>
    <br /><br />

    <fieldset>
        <legend>Redirecionar para:</legend>
        <label>
            <input type="checkbox" name="redirecionar[]" value="home">
        </label> Home

        <br />

        <label>
            <input type="checkbox" name="redirecionar[]" value="contato">
        </label> Contato

        <br />

    </fieldset>

    <hr>

    <input type="submit" value="Enviar">

</form>
</div>
</body>

</html>


