<?php
/**
 * Created by PhpStorm.
 * User: Sagres 3
 * Date: 05/04/2019
 * Time: 10:24
 */
require_once "vendor/autoload.php";

$app = new Silex\Application;

$app['debug'] = true;

$app->get('/',  function (){
    $hellopakcage = new \Marcelo\SON\HelloPackage;
    return $hellopakcage->getHello();
});

$app->run();
