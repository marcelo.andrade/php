<?php
/**
 * Created by PhpStorm.
 * User: Sagres 3
 * Date: 09/04/2019
 * Time: 10:00
 */

namespace Marcelo\SON;


class HelloPackageTest extends \PHPUnit_Framework_TestCase
{
    public function testGetHello() {
       $hello = new HelloPackage();

       $string = $hello->getHello();

       $this->assertNotNull($string);
    }
}